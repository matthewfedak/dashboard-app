import React, { Component } from 'react';
import ReportView from './ReportView';
import ButtonPrev from './ButtonPrev';
import ButtonNext from './ButtonNext';
import ButtonSettings from './ButtonSettings';

import './App.css';
import './Button.css';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentFrame: 1,
            totalFrames: this.props.reportFrames.length,
            settingsPageOpen: false,
            windowWidth: window.outerWidth,
            windowHeight: window.innerHeight,
            reportsWidth: (this.props.reportFrames.length * window.outerWidth)
        };

        this.goToNextFrame = this.goToNextFrame.bind(this);
        this.goToPreviousFrame = this.goToPreviousFrame.bind(this);
        this.toggleSettingsPage = this.toggleSettingsPage.bind(this);
    }

    goToNextFrame(e) {
        if (this.state.currentFrame === this.state.totalFrames) {
            return;
        }
        this.setState({
            currentFrame: this.state.currentFrame + 1
        });
    }

    goToPreviousFrame(e) {
        if (this.state.currentFrame === 1) {
            return;
        }
        this.setState({
            currentFrame: this.state.currentFrame - 1
        });
    }

    toggleSettingsPage(e) {
        if (this.state.settingsPageOpen) {
            // close the settings page
            return true;
        }
        // open the settings page
        return true;
    }

    render() {
        return (
            <div>
                <ReportView
                    reportFrames={this.props.reportFrames}
                    reportsWidth={this.state.reportsWidth}
                    currentFrame={this.state.currentFrame}
                    windowWidth={this.state.windowWidth}
                    windowHeight={this.state.windowHeight}
                />
                <div className="MenuBar">
                    <ButtonPrev
                        action={this.goToPreviousFrame}
                        disabled={(this.state.currentFrame === 1) ? true : false}
                    />
                    <ButtonNext
                        action={this.goToNextFrame}
                        disabled={(this.state.currentFrame === this.state.totalFrames) ? true : false}
                    />
                    <ButtonSettings
                        action={this.toggleSettingsPage}
                    />
                </div>
            </div>
        );
    }
}

export default App;