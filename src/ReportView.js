import React, { Component } from 'react';
import './ReportView.css';
import ReportFrame from './ReportFrame';

class ReportView extends Component {

    render() {

        let offset = (this.props.currentFrame - 1 ) * this.props.windowWidth;

        let reports = [];

        this.props.reportFrames.forEach((report) => {
            reports.push(
                <ReportFrame
                    report={report} key={report.id}
                    windowWidth={this.props.windowWidth}
                    windowHeight={this.props.windowHeight}
                />
            );
        });

        return (
            <div
                className="ReportView"
                style={{marginLeft: '-' + offset + 'px', width: this.props.reportsWidth}}
            >
                {reports}
            </div>
        );
    }
}

export default ReportView;