import React, { Component } from 'react';

class ButtonPrev extends Component {

    render() {
        return (
            <div className={this.props.disabled ? 'disabled btn' : 'btn'}>
            	<span onClick={this.props.action}>
    				 &laquo; Previous
            	</span>
        	</div>
        );
    }
}

export default ButtonPrev;