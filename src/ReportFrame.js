import React, { Component } from 'react';

class ReportFrame extends Component {
    render() {
        return (
            <iframe
                src={this.props.report.src}
                id={this.props.report.src}
                width={this.props.windowWidth}
                height={this.props.windowHeight}
            >
            </iframe>
        );
    }
}

export default ReportFrame;