import React, { Component } from 'react';

class ButtonSettings extends Component {

    render() {
        return (
            <div className="btn settings">
            	<span onClick={this.props.action}>
            		Settings
        		</span>
    		</div>
        );
    }
}

export default ButtonSettings;