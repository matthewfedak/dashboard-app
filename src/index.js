import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

const REPORTFRAMES = [
    {
        "id":"sub85",
        "name": "sub85",
        "src":"http://htmlgoodies.com"
    },
    {
        "id": "da",
        "name": "da",
        "src": "http://www.designalchemy.co.uk/"
    },
    {
        "id":"matthewfedak",
        "name": "matthewfedak",
        "src":"http://www.matthewfedak.co.uk/"
    },
    {
        "id":"krankikom",
        "name": "krankikom",
        "src":"http://www.krankikom.de/"
    }
];

ReactDOM.render(
	<App reportFrames={REPORTFRAMES}/>,
	document.getElementById('root')
);