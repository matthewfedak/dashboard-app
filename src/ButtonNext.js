import React, { Component } from 'react';

class ButtonNext extends Component {

    render() {
        return (
            <div className={this.props.disabled ? 'disabled btn' : 'btn'}>
            	<span onClick={this.props.action} >
            		Next &raquo;
            	</span>
            </div>
        );
    }
}

export default ButtonNext;